# **BENTENGAN**

## Intro
Bentengan is an adaptation of a traditional game from indonesia called Benteng-bentengan. Bentengan was developed during the pandemic, to allow childrens to know about the traditional game as purpose in mind.

Bentengan is a tag-team which can be played from 2 to 8 player. There's little different with the real game which the power is decreasing overtime, the prisoner mechanism, and best of 3 mechanism so that the game will be easier to play and understand


<br/>

## Platform
<img src="Readme Asset/Windows_rgb_Blue_D.png" width="15%">

We choose to use windows as our platform for prototyping bacause of it is easier to develop than mobile.


<br/>

## Tools
     
- **Game Engine**
<img src="Readme Asset/Unity-Logo.png" width="10%">

Unity is choosen as (version 2019.4.22) our development engine because of its reliability and development experience using this engine.

- **Hosting**
<img src="Readme Asset/digitalocean-logo.jpg" width="15%">

We choose Digital Ocean as our hosting because of its great service and easy to use management interface. Also Digital Ocean give you free trial after registering your account.

<br/>

# **Gameplay**

- **Log in**
<img src="Readme Asset/Login.PNG" width="75%">

<br/>
<br/>
<br/>

- **Select Team Scene**

<img src="Readme Asset/selectTeam.PNG" width="75%">
<br/>
<br/>
<br/>

- **Prison Gate Mechanism**

<img src="Readme Asset/a.gif" width="75%">
<br/>
<br/>
<br/>

- **Capture Enemy Base & Change Round**

<img src="Readme Asset/b.gif" width="75%">
<br/>
<br/>
<br/>

- **Win Condition**

<img src="Readme Asset/c.gif" width="75%">


<br/>

# **Diagram**
<img src="Readme Asset/flow.png">

<br/>

# **Credits**
- Programmer : [Jatayu Muhammad Wicaksono](https://gitlab.com/jatayumuw)
- Programmer : [Faiddurohman](https://gitlab.com/faid12)
- Art        : [Estúdio Vaca Roxa](https://estudiovacaroxa.com.br/)
